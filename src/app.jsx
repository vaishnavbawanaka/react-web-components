import React from 'react'
import HelloWorld from "./components/HelloWorld.Component";

const App = () => {
  return (
    <h1>
      <HelloWorld />
    </h1>
  )
}

export default App